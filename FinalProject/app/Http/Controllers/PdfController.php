<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PDF;
use App\Artikel;

class PdfController extends Controller
{
    public function download($id){
        
        set_time_limit(300);
        $artikel = Artikel::find($id);

        // view()->share('artikel',$artikel);
        $pdf = PDF::loadView('pdf',compact('artikel'));
        $pdf->setPaper('a4', 'portrait');   //horizontal
        return $pdf->download('artikel.pdf');
       
    }
}
