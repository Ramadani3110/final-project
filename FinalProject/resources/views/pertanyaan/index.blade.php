@extends('pages.forum')

@push('style_data-tables')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('pertanyaan')
<div class="card">
      <div class="card-header">
        <h3 class="card-title">#Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th width="5%">No</th>
            <th>Tanggal Posting</th>
            <th width="25%">Judul</th>
            <th>Isi</th>
            <th width="20%">Ditanyakan Oleh</th>
            <th width="10%">Aksi</th>
          </tr>
          </thead>
          <tbody>
        @forelse ($pertanyaan as $key => $item)
          <tr>
            <td>{{ $key+1}}</td>
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->judul }}</td>
            <td>{{ Str::limit($item->isi, 35) }}</td>
            <td>{{ $item->user->name }}</td>
            <td><a href="/pertanyaan/{{ $item->id }}" class="btn btn-primary text-decoration-none">Lihat Diskusi</a></td>
        @empty
            Belum ada pertanyaan
        @endforelse
          </tr>
  
        </tbody>
        
        </table>
      </div>
      <!-- /.card-body -->
    </div>



@endsection

@push('script_data-tables')

  <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush