<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Landing page template built with HTML and Bootstrap 4 for presenting training courses, classes, workshops and for convincing visitors to register using the form.">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
<title>Kategori</title>
    
<!-- Styles -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,600,700,700i&display=swap" rel="stylesheet">
<link href="{{asset('corso/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('corso/css/fontawesome-all.css')}}" rel="stylesheet">
<link href="{{asset('corso/css/swiper.css')}}" rel="stylesheet">
<link href="{{asset('corso/css/magnific-popup.css')}}" rel="stylesheet">
<link href="{{asset('corso/css/styles.css')}}" rel="stylesheet">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    
    <!-- end of navigation -->

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">

    <!-- Text Logo - Use this if you don't have a graphic logo -->
    <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Corso</a> -->
    
    <!-- Mobile Menu Toggle Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-awesome fas fa-bars"></span>
        <span class="navbar-toggler-awesome fas fa-times"></span>
    </button>
    <!-- end of mobile menu toggle button -->

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-auto">
            <!-- Dropdown Menu -->          
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle page-scroll" href="#" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:rgb(78, 81, 82)">{{ Auth::user()->email }}</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/"><span class="item-text">BERANDA</span></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/dashboard"><span class="item-text">DASHBOARD</span></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/artikel"><span class="item-text">ARTIKEL</span></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/kategori"><span class="item-text">KATEGORI</span></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/pertanyaan"><span class="item-text">FORUM</span></a>
                </div>
            </li>
            <!-- end of dropdown menu -->

        </ul>
    </div>
</nav> <!-- end of navbar -->
<!-- end of navigation -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            <h1>Kategori</h1>
                            <p class="p-large">Kamu bisa melihat kategori disini agar lebih mudah dalam mencari solusi dari permasalahan yang kamu cari</p>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-xl-7">
                        <div class="image-container">
                            <div class="img-wrapper">
                            </div> <!-- end of img-wrapper -->
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
<!-- Start Forum -->

     <!-- Kategori -->
     <div class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="mb-5 title text-center">List Kategori</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div>
                @yield('kategori')
            </div>
        </div> <!-- end of container -->
        <!-- end of kategori -->
    </div>

    <script src="{{asset('corso/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{asset('corso/js/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="{{asset('corso/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
    <script src="{{asset('corso/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{asset('corso/js/jquery.countdown.min.js')}}"></script> <!-- The Final Countdown plugin for jQuery -->
    <script src="{{asset('corso/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{asset('corso/js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{asset('corso/js/validator.min.js')}}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{asset('corso/js/scripts.js')}}"></script> <!-- Custom scripts -->
</body>

</html>
